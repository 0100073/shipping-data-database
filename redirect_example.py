import flask
from flask import render_template
from flask_util_js import FlaskUtilJs
import sys

app = flask.Flask(__name__)
app.config['DEBUG'] = True
fujs = FlaskUtilJs(app)

@app.route('/')
def hello():
    return 'Hello, Citizen of CS257.'

@app.route('/redirectTest/')
def redirectTest():
    return render_template('redirect_test.html')

@app.route('/greet/<person>/')
def greet(person):
    return render_template('greet.html',
                           person=person)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: {0} host port'.format(sys.argv[0]), file=sys.stderr)
        exit()

    host = sys.argv[1]
    port = sys.argv[2]
    app.run(host=host, port=int(port))