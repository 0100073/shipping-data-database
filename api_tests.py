import unittest
import api

class APITester(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    # Ensures that the function returns only results from the correct U.S. state
    def testResultsFromCorrectState(self):
        testResults = api._displaySearchResults({'state': 'CT'})
        self.assertTrue((testResults[1]['Port'] == 'New London' and testResults[0]['Port'] == 'New Haven')
                        or (testResults[1]['Port'] == 'New Haven' and testResults[0]['Port'] == 'New London'))

    # Ensures that the function returns the right number of results for each state
    def testReturnsAllResultsFromState(self):
        testResults = api._displaySearchResults({'state': 'CT'})
        self.assertEqual(len(testResults), 2)

    # Ensures that only results with the correct min capacity are returned
    def testValidMinimumCapacities(self):

        testResults = api._displaySearchResults({'minAllCapacity': '13000000'})
        for port in testResults:
            self.assertTrue(port['AllCapacity'] >= 13000000)

    # Ensures that only results with the correct max capacity are returned
    def testValidMaximumCapacities(self):
        testResults = api._displaySearchResults({'maxAllCapacity': '13000000'})
        for port in testResults:
            self.assertTrue(port['AllCapacity'] <= 13000000)

    # Ensures that only results with the correct min number of calls are returned
    def testValidMinimumCalls(self):
        testResults = api._displaySearchResults({'minAllCalls': '500'})
        for port in testResults:
            self.assertTrue(port['AllCalls'] >= 500)

    # Ensures that only results with the correct max number of calls are returned
    def testValidMaximumCalls(self):
        testResults = api._displaySearchResults({'maxAllCalls': '750'})
        for port in testResults:
            self.assertTrue(port['AllCalls'] <= 750)

    # Tests the ability to display a single, specific port
    def testDisplayPort(self):
        portDictionary = api._displayPortInformation('New_Orleans')
        self.assertEqual(portDictionary["State"], 'LA')
        self.assertEqual(portDictionary['ContainerDWT'], 21700448)

if __name__ == '__main__':
    unittest.main()