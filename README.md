# Shipping Data Database and Web Interface Project #

This two person project was used as a basic introduction to SQL databases and web access. 
The project uses Flask to build a web interface for a PostgreSQL database that contained information on commercial ports in the U.S.
api.py contains the code that sets up the web interface, the Jinja templates are stored in the templates folder, our our CSS file is stored in the static folder.