import flask
from flask import render_template, request
import json
import sys
import psycopg2
import getpass

app = flask.Flask(__name__)
global connection
connection = psycopg2.connect(database="robertsk2", user="robertsk2", password="smile567desktop")

# colList is a list (in order) of the titles of all of the columns in our database
global colList
colList = ['Port', 'State', 'AllCalls', 'AllCapacity', 'TankerCalls', 'TankerCapacity', 'ContainerCalls',
               'ContainerDWT', 'ContainerTEU', 'GasCalls', 'GasDWT', 'GasGas', 'RoRoCalls', 'RoRoDWT', 'BulkCalls',
               'BulkDWT', 'GenCalls', 'GenDWT']

@app.route('/')
def displayHome():
    return render_template('home.html')

@app.route('/home/')
def displayHomeFromLink():
    return displayHome()

@app.route('/about/')
def displayAboutPage():
    return render_template('README.html')

@app.route('/glossary/')
def displayGlossary():
    """
    Displays a webpage containing information about the dataset, including
    a glossary of shipping-related terms like "roll-on roll-off" and "deadweight tonnage".

    Code:
    return render_template('glossary.html')
    """
    return render_template('glossary.html')

@app.route('/newSearch/')
def displayNewSearch():
    return render_template('newSearch.html')

@app.route('/sql/')
def displaySQLPage():
    return render_template('sqlscript.html')

@app.route('/portem/')
def displayPortemPage():
    return render_template('portem.html')


@app.route('/search/')
def displaySearchResults():
    try:
        # If there is no query, return the newSearch page
        if not request.args:
            print("not request.args.")
            return render_template('newSearch.html')
        else:
            queryDictionary = request.args
            portList = sorted(_displaySearchResults(queryDictionary), key = returnName_key)
    except Exception as e:
        print(e)
        return render_template('errorPage.html')


    try:
        # This is passed to the the search results template and used to indicate what state the user searched for
        selectedDictionary = {queryDictionary['state']:'selected'}
        return render_template('search.html', ports=portList, lastQuery=queryDictionary, selected=selectedDictionary)
    except Exception as e:
        return render_template('errorPage.html')

# This is used as the sorting key by the built-in python sorted() function to sort
# the search results alphabetically by port name.
def returnName_key(portDictioinary):
    return portDictioinary['Port']

def _displaySearchResults(queryDictionary):
    """
    calls a method to operate on searchString, reformatting it as dictionary of
    search parameters matched with input.  Then this dictionary is passed to a
    template to be displayed as search results on an HTML page.

    Example:
    '/search/?state=AK&minAllCalls=100&maxAllCalls=300' returns
    [{'port': 'Anchorage', 'calls': 216, 'all_capacity' : 5170916, 'tanker_calls' : 9, 'tanker_capacity' : 476000,
        'container_calls' : 102, 'container_dwt' : 2177803, 'container_teu' : 174576, 'gas_calls' : 0, 'gas_dwt' : 0,
        'gas_gas' : 0, 'roro_calls' :  94, 'roro_dwt' : 2274739, 'bulk_calls' : 0, 'bulk_dwt' : 153,013,
        'gen_calls' : 6, 'gen_dwt' : 89361}
     {'port': 'Valdez', 'calls': 263, 'all_capacity' : 33418667, 'tanker_calls' : 260, 'tanker_capacity' : 33378365,
        'container_calls' : 1, 'container_dwt' : 21292, 'container_teu' : 1712, 'gas_calls' : 0, 'gas_dwt' : 0,
        'gas_gas' : 0, 'roro_calls' : 0, 'roro_dwt' : 0, 'bulk_calls' : 0, 'bulk_dwt': 0, 'gen_calls' : 2, 'gen_dwt'
        : 19010}]
    """

    try:
        connection = psycopg2.connect(database="robertsk2", user="robertsk2", password="smile567desktop")
    except Error as e:
        print(Error)
        raise e

    # At this point, we should not have an empty query
    if not queryDictionary:
        print("EMPTY QUERY")
        raise ValueError("Empty Query Error")

    try:
        query = processQueryDictionary(queryDictionary, colList)
        cur = connection.cursor()
        cur.execute(query)
        portList = []
        for row in cur:
            # converts the tuple returned by the database into a dictionary
            rowAsDictionary = dictionarify(row, colList)
            portList.append(rowAsDictionary)
        connection.close()
        return portList
    except Exception as e:
        connection.close()
        raise e

@app.route('/port/<portName>/')
def displayPortInformation(portName):
    try:
        portDictionary = _displayPortInformation(portName)
        return render_template('port.html', port=portDictionary)
    except Exception as e:
        return render_template('errorPage.html')

def _displayPortInformation(portName):
    """
    Looks up the information on a specified port and then displays it on a specialized search results page.

    Example:
    '/port/anchorage/' should return a page with all of Anchorage's information on it.
    """

    try:
        connection = psycopg2.connect(database="robertsk2", user="robertsk2", password="smile567desktop")
    except Error as e:
        print(Error)
        raise e

    try:
        portName = portName.replace("_", " ")
        query = "SELECT * FROM portCalls WHERE Port=%s;"
        cur = connection.cursor()
        cur.execute(query, (portName,))
        rowList= []
        for row in cur:
            rowList.append(row)
        if len(rowList) != 1:
            #print("ROW LIST! " + rowList)
            raise ValueError('Found multiple or no ports with the given name.')
        # rowList[0] is the tuple containing the information of the only port in rowList
        portDictionary = dictionarify(rowList[0], colList)
        connection.close()
        return portDictionary
    except ValueError as e:
        connection.close()
        raise e

def processQueryDictionary(queryDictionary, colList):
    queryString = "SELECT * FROM portcalls"
    constraintList = []
    for key in queryDictionary:

        # If-Else to determine whether the query item is regarding the state
        if isAnInteger(queryDictionary[key]):
            # colList contains a list of each of the data types in the database
            # It's first two entries are Port and State.
            if key[:3] == 'min' and key[3:] in colList[2:]:
                constraint = key[3:] + ">=" + queryDictionary[key]
                constraintList.append(constraint)
            elif key[:3] == 'max' and key[3:] in colList[2:]:
                constraint = key[3:] + "<=" + queryDictionary[key]
                constraintList.append(constraint)
        # and queryDictionary[key] != '' prevents the program from returning no search results if the user
        # does not select a specific state to search in.
        elif key == 'state' and queryDictionary[key] != '':
            constraint = "State=" + "'" + queryDictionary[key] + "'"
            constraintList.append(constraint)
    #print(constraintList)

    if len(constraintList) == 0:
        raise ValueError("Query contained no valid constraints")
    else:
        queryString = queryString + " WHERE " + constraintList[0]
    for i in range(1, len(constraintList)):
        queryString = queryString + " AND " + constraintList[i]
    queryString = queryString + ";"
    return(queryString)

# Turns a tuple of query results into a dictionary using colList, which must be a complete and ordered
# list of the names of all of the columns in the database table.
def dictionarify(portTuple, colList):
    portDictioinary = {}
    if len(portTuple) != len(colList):
        raise ValueError("The database did not return results in (only) the expected categories")

    for i in range(len(colList)):
        portDictioinary[colList[i]] = portTuple[i]

    return portDictioinary


def isAnInteger(possibleIntegerString):
    try:
        number = int(possibleIntegerString)
        return True
    except ValueError:
        return False


def main():
    if len(sys.argv) != 3:
        print('Usage: {0} host port.format(sys.argv[0]), file=sys.stderr')
        exit()
    host = sys.argv[1]
    port = sys.argv[2]
    app.run(host=host, port=port)

if __name__ == '__main__':
    main()