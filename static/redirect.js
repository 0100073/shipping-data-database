/**
 * Created by ealexander on 2/14/2017.
 */

// This function takes advantage of the greet_url_temp variable defined in redirect_test.html
// We will replace the placeholder value in that URL to create our new one
var jinja_hack_redirect = function() {
    var name = document.getElementById('nameInput').value;
    var greet_url = greet_url_temp.replace('NOTAPERSON', name);
    window.location = greet_url;
};

// This function makes use of the flask_util_js library
// which allows us to create Flask URLs directly in Javascript.
var flask_util_redirect = function() {
    var name = document.getElementById('nameInput').value;
    var greet_url = flask_util.url_for('greet', {'person': name});
    window.location = greet_url;
};
