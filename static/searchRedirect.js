/**
 * Created by ratzlaffa on 2/15/17.
 */


// This function takes all of the search parameters we enter in the search box and plugs them into a URL
var flask_util_redirect = function() {
    var name = document.getElementById('nameInput').value;
    var greet_url = flask_util.url_for('greet', {'person': name});
    window.location = greet_url;
};
