DROP TABLE IF EXISTS portCalls;
CREATE TABLE portCalls (
  Port VARCHAR,
  State CHARACTER(2),
  AllCalls INTEGER,
  AllCapacity INTEGER,
  TankerCalls INTEGER,
  TankerCapacity INTEGER,
  ContainerCalls INTEGER,
  ContainerDWT INTEGER,
  ContainerTEU INTEGER,
  GasCalls INTEGER,
  GasDWT INTEGER,
  GasGas INTEGER,
  RoRoCalls INTEGER,
  RoRoDWT INTEGER,
  BulkCalls INTEGER,
  BulkDWT INTEGER,
  GenCalls INTEGER,
  GenDWT INTEGER
);